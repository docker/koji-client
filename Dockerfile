FROM igwn/base:el9

LABEL name="Koji Client"
LABEL maintainer="Adam Mercer <adam.mercer@ligo.org>"
LABEL support="Best Effort"

# install required packages
RUN dnf makecache && \
      dnf -y update && \
      dnf -y install \
        cpio \
        emacs \
        git-lfs \
        git \
        koji \
        krb5-workstation \
        less \
        mock \
        rpm-build \
        rpmdevtools \
        rpmlint \
        sudo \
        vim \
        wget && \
      dnf clean all

# setup environment
COPY /environment/koji.conf /etc/koji.conf
COPY /environment/krb5.conf /etc/krb5.conf
COPY /environment/mock /etc/mock
